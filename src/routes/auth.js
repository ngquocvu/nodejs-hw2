import express from 'express';
import {login, register} from '../controllers/user.js';

const authRouter = new express.Router();
authRouter.post('/register', register);
authRouter.post('/login', login);

export default authRouter;
